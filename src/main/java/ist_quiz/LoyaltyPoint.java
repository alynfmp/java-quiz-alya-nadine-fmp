package ist_quiz;

public class LoyaltyPoint {

    private int remainingPoint;
    private String message;
    private String redeemVoucher;
    public LoyaltyPoint(){
    }
    public LoyaltyPoint(int remainingPoint, String message, String redeemVoucher){
        this.remainingPoint =remainingPoint;
        this.message = message;
        this.redeemVoucher = redeemVoucher;
    }

    public String getRedeemVoucher() {
        return redeemVoucher;
    }

    public void setRedeemVoucher(String redeemVoucher) {
        this.redeemVoucher = redeemVoucher;
    }

    public int getRemainingPoint() {
        return remainingPoint;
    }

    public void setRemainingPoint(int remainingPoint) {
        this.remainingPoint = remainingPoint;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoyaltyPoint convertPoint(int point){
        if(point >= 800){
            message = "Point anda : "+point+"p dapat ditukar dengan voucher pulsa Rp.100.000 ";
            remainingPoint = point - 800;
            redeemVoucher = "100000";
        } else if (point >= 400) {
            message = "Point anda : "+point+"p dapat ditukar dengan voucher 50.000";
            remainingPoint = point - 400;
            redeemVoucher = "50000";
        } else if (point >= 200) {
            message = "Point anda : "+point+"p dapat ditukar dengan voucher 25.000";
            remainingPoint = point - 200;
            redeemVoucher = "25000";
        } else if (point >= 100) {
            message = "Point anda : "+point+"p dapat ditukar dengan voucher 10.000";
            remainingPoint = point - 100;
            redeemVoucher = "10000";
        } else {
            message = "Point anda : "+point+"p tidak cukup untuk ditukar dengan voucher yang ada";
            remainingPoint = point;
            redeemVoucher = "";
        }

        return new LoyaltyPoint(remainingPoint,message,redeemVoucher);
    }

}
