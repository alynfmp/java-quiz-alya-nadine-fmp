package ist_quiz;

import java.util.*;

public class LoyaltyPointApplication {

    public static LoyaltyPoint loyaltyPoint = new LoyaltyPoint();

    public static void main(String[] args) throws Exception {
        String line = "----------------------------------------------";
        System.out.println(line);
        System.out.println("         Feature Loyalty Point !");
        System.out.println(line);
        System.out.println("           List Voucher Pulsa   ");
        System.out.println("- Voucher Pulsa Rp.10.000  = 100 point ");
        System.out.println("- Voucher Pulsa Rp.25.000  = 200 point ");
        System.out.println("- Voucher Pulsa Rp.50.000  = 400 point ");
        System.out.println("- Voucher Pulsa Rp.100.000 = 800 point ");
        System.out.println("      ");

        Menu m = new Menu();
        m.mainMenu();


    }
}
