package ist_quiz;

import java.util.*;

public class Menu {

    public static LoyaltyPoint loyaltyPoint = new LoyaltyPoint();
    public void mainMenu() throws Exception {
        try {
            String line = "----------------------------------------------";
            System.out.println(line);
            System.out.println("              TEST FEATURE");
            System.out.println(line);
            System.out.println("1. Menentukan Voucher Dengan Jumlah Point terbesar");
            System.out.println("2. Menghitung sisa poin setelah di tukar dgn point terbesar jika poin yg dimiliki adalah 1000p");
            System.out.println("3. Tukar semua point prioritas point terbesar");
            System.out.println("0. Exit");
            Scanner input = new Scanner(System.in);
            System.out.println("Silahkan masukkan pilihan anda :");
            if (!input.hasNextInt()) {
                System.out.println("Anda harus memasukkan angka");
                this.promptEnterKey();
                this.mainMenu();
            }
            int choice = input.nextInt();
            switch (choice) {
                case 1 -> {
                    int point = this.point();
                    LoyaltyPoint pointInput1 = loyaltyPoint.convertPoint(point);
                    System.out.println(line);
                    System.out.println("Test 1 : " + pointInput1.getMessage());
                    System.out.println(line);
                    this.promptEnterKey();
                }
                case 2 -> {
                    int point = this.point();
                    LoyaltyPoint pointInput2 = loyaltyPoint.convertPoint(point);
                    System.out.println(line);
                    System.out.println("Test 2 : " + "Sisa point anda : " + pointInput2.getRemainingPoint());
                    System.out.println("       : Jika point anda = " + point + ", maka" + " sisa point anda : " + pointInput2.getRemainingPoint());
                    System.out.println(line);
                    this.promptEnterKey();
                }
                case 3 -> {
                    int point = this.point();
                    LoyaltyPoint pointTest3 = TestThree(point);
                    System.out.println(line);
                    System.out.println(pointTest3.getMessage());
                    System.out.println(line);
                    this.promptEnterKey();
                }
                case 0 -> {
                    System.out.println(line);
                    System.out.println("Finish~");
                    System.out.println(line);
                    System.exit(0);
                }
                default -> {
                    System.out.println("Pilihan tidak ditemukan !");
                    System.out.println("Silahkan tekan enter untuk kembali ke main menu");
                    this.promptEnterKey();
                }
            }
            this.mainMenu();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    //TEST FEATURE NO 3
    public static LoyaltyPoint TestThree(Integer inputPoint) {
        int remainingPoint = inputPoint;
        int voucher100 = 0, voucher50 = 0, voucher25 = 0, voucher10 = 0;
        while (remainingPoint > 100) {
            LoyaltyPoint voucherTestRemainingPoint = loyaltyPoint.convertPoint(remainingPoint);
            switch (voucherTestRemainingPoint.getRedeemVoucher()) {
                case "100000" -> voucher100 = 1;
                case "50000" -> voucher50 = 1;
                case "25000" -> voucher25 = 1;
                case "10000" -> voucher10 = 1;
            }
            remainingPoint = voucherTestRemainingPoint.getRemainingPoint();
        }
        String message;
        if (remainingPoint != inputPoint) {
            message = ("Test 3 : Point anda : " + inputPoint + "p , " +
                    "Anda dapat menukarkannya dengan "
                    + voucher100 + " voucher 100.000, "
                    + voucher50 + " voucher 50.000, "
                    + voucher25 + " voucher 25000, "
                    + voucher10 + " voucher 10000, "
                    + "sisa point : " + remainingPoint);
        } else {
            message = ("Test 3 : Point anda : " + inputPoint + "p , " +
                    "maaf point anda tidak cukup untuk ditukar dengan voucher yang ada");
        }
        return new LoyaltyPoint(remainingPoint, message, null);
    }
    public void promptEnterKey(){
        System.out.println("Tekan \"ENTER\" untuk lanjut...");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }
    public int point() throws Exception {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan total point anda :");
        if (!input.hasNextInt()) {
            System.out.println("Anda harus memasukkan angka");
            this.promptEnterKey();
            this.mainMenu();
        }
        int inputPoint = input.nextInt();
        if (inputPoint <= 0) {
            System.out.println("point harus > 0");
            this.promptEnterKey();
            this.mainMenu();
        }
        return inputPoint;
    }

}
